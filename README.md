riotgames-testapp
=================

This repository contains a batch process which reads a directory of JSON-encoded list of Players, and outputs:

1. A CSV-formatted list of all players from the input data with more than five wins, and
2. A JSON-formatted list of all players from the input data that contains a list of suggested friends for each player.

### Dependencies

* Java 6 (or higher)

This will be installed automatically if using Chef (see below).

### Building from source

If git is installed on the system, `git clone https://bitbucket.org/danalstadt/riotgames-testapp.git` will clone a copy of this repository into a new directory called `riotgames-testapp`.

`mvn package` compiles the source to a .jar Java executable. The .jar build artifact is placed in the `target` directory.

### Basic Command-line Usage

```
java -jar TestApp-0.0.1-SNAPSHOT.jar {input directory} {winners output file path} {suggested friends output file path}
```

This will read all of the files in the input directory, parse them as JSON, and output a winners file and a suggested friends file to the respective locations specified.

## Installation

When installed properly, this process will run twice daily. There are a a few options for provisioning a new server. These have all been tested on Ubuntu 13.10, but may also apply to other distributions. The three methods listed below all assume that this repository has been cloned or copied to the server in question.

### Using the install script (recommended)

After the dependencies have been installed, `scripts/install.sh` can be run, either by the root user or using sudo (e.g., `sudo scripts/install.sh`) to install in the proper location. This copies the executable to an appropriate location on the system, and creates directories for input and output files.

### Using Chef Solo (recommended for users with Chef experience)

A Chef cookbook and configuration are included for configuring this process. This cookbook should also work with Chef Server, but such configuration is outside the scope of this documentation.

Chef Solo must be installed to use this chef cookbook. A script is provided for Debian/Ubuntu systems to install Chef Solo. Simply run `chef/install-chef.sh` as root to install the current version of ruby and chef-solo on the server. On non-Debian/Ubuntu systems, the process will vary, but will involve installing Ruby >= 1.9, and the `chef` rubygem (http://rubygems.org/gems/chef). Experienced users can also use the `knife bootstrap` incantation.

With chef installed, from the `chef` directory, run `chef-solo -c solo.rb -j solo.json` as root. This will build and install the process and its dependencies.

### Manually

You can manually configure the process to run twice daily using a cron task. Here's a crontab entry for running the process at noon and midnight (in the server's time zone):

```
0 0,12 * * * java -jar /path/to/testapp.jar /path/to/input-directory /path/to/winners.csv /path/to/suggestedFriends.json
```

Use `crontab -e` to edit your user's crontab. You will need to adjust the paths in the above command to reflect where the .jar file, input files, and desired output files reside on your system. This command will run at noon and midnight in the server's timezone.

## Confirming Proper Installation

If installed correctly, the process will run at noon and midnight every day (in the server's timezone - run `date` to confirm the current date/time on the server). At this time, output files will be written to the default locations specified below (or the custom locations specified to `install.sh`). Output will be written to `/var/log/riotgames/testapp.log` each time the process runs that details which files are read and output.

## Default install locations

By default, the Chef cookbook and the `scripts/install.sh` script place/expect files in the following locations on the system:

* `/var/riotgames/testapp/input` - input JSON player files
* `/var/riotgames/testapp/output` - output files
* `/opt/riotgames/testapp/` - .jar Java executable
* `/var/log/riotgames/testapp.log` - log output from the process
* `/var/src/riotgames-testapp` - source files (this repository)

`scripts/install.sh` prompts the user to provide custom input and output locations if desired.

## Repository Layout

* `README.md` - this file
* `pom.xml` - maven project configuration
* `chef` - chef cookbook and configuration
* `src` - java source files
* `test` - unit tests
* `scripts` - installation scripts
* `TestInput` - (modified) Riot-provided input files
* `SampleInput` - my test input files, used in unit testing
* `Target` - compiled code output
* `javadoc` - auto-generated javadoc documentation