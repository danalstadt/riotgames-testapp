#!/usr/bin/env bash

UNMET_DEPENDENCY=0
DEFAULT_INPUT_DIR="/var/riotgames/testapp/input"
DEFAULT_OUTPUT_DIR="/var/riotgames/testapp/output"

#get current user, who will have ownership of output files
if [[ -z $SUDO_USER ]]; then
	USER=`whoami`
else
	USER="$SUDO_USER"
fi

#we need to be root
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "This script must be run with root privileges."
    exit 1
fi

#cd to the root of the source repo
cd "$(dirname "$0")"
cd ..

#check dependencies
if ! type "java" &> /dev/null; then
	echo "ERROR: java is not installed. Please install java and rerun this script."
	UNMET_DEPENDENCY=1
fi
if [[ $UNMET_DEPENDENCY != 0 ]]; then
	echo "Unmet dependencies. Please rectify and try again."
	exit 2
fi

#get input and output directories from user (with defaults)
read -p "Directory for input files [$DEFAULT_INPUT_DIR]: " INPUT_DIR
INPUT_DIR=${INPUT_DIR:-$DEFAULT_INPUT_DIR}
read -p "Directory for output files [$DEFAULT_OUTPUT_DIR]: " OUTPUT_DIR
OUTPUT_DIR=${OUTPUT_DIR:-$DEFAULT_OUTPUT_DIR}
CRONTAB_LINE="0 0,12 * * * root cd /opt/riotgames/testapp/ && java -jar testapp.jar $INPUT_DIR ${OUTPUT_DIR}/winners.csv ${OUTPUT_DIR}/suggestedFriends.json >> /var/log/riotgames/testapp.log 2>&1 && chown -R ${USER} ${OUTPUT_DIR}"

#install the jar and set up input, output, and log directories
echo "Installing..."
mkdir -p /opt/riotgames/testapp
mkdir -p $INPUT_DIR
mkdir -p $OUTPUT_DIR
mkdir -p /var/log/riotgames
touch /var/log/riotgames/testapp.log
chown -R $USER:root /var/riotgames/testapp
cp -f target/TestApp-0.0.1-SNAPSHOT.jar /opt/riotgames/testapp/testapp.jar 

#configure cron task
echo "Setting up cron task..."
echo "$CRONTAB_LINE" > /etc/cron.d/riotgames-testapp

echo "Done."

