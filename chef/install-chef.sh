#!/bin/bash

# Installs ruby and chef-solo on the server
# This needs to run as root
# Derived from install.sh at http://www.opinionatedprogrammer.com/2011/06/chef-solo-tutorial-managing-a-single-server-with-chef/

if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "This script must be run with root privileges."
    exit 1
fi

chef_binary=/var/lib/gems/1.9.1/bin/chef-solo
export DEBIAN_FRONTEND=noninteractive
aptitude update &&
apt-get -o Dpkg::Options::="--force-confnew" \
	--force-yes -fuy dist-upgrade &&
aptitude install -y ruby1.9.1 ruby1.9.1-dev make &&
sudo gem install --no-rdoc --no-ri chef