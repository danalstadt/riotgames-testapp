#
# Cookbook Name:: testapp
# Recipe:: default
#

checkout_path = "/var/src/riotgames-testapp"

case node[:platform]
  when "ubuntu", "debian"
    package "default-jre"
  when "centos", "redhat"
    package "java"
end

directory checkout_path do
  recursive true
end

git "riotgames-testapp" do
  repository "https://bitbucket.org/danalstadt/riotgames-testapp.git"
  reference "master"
  destination checkout_path
end

execute "setup-task" do
  command "#{checkout_path}/scripts/install.sh"
end
