package com.riotgames.devops.testapp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.text.SimpleDateFormat;
import java.util.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * <p>
 * This application simulates a job that asynchronously processes player data
 * (stored in a JSON file passed in as the first command-line argument) and
 * writes the output into separate files, given as the second and third arguments.
 * </p>
 * <p>
 * The application generates a comma separated
 * value file that contains the following columns for all players with more than
 * 5 wins:
 * <ol>
 * <li>player ID ({@link PlayerDTO#getPlayerId()})</li>
 * <li>player alias ({@link PlayerDTO#getAlias()})</li>
 * <li>Number of wins ({@link PlayerDTO#getNumWins()})</li>
 * <li>Last played date ({@link PlayerDTO#getLastPlayedDate()}) <strong>in a <code>11/13/2013 12:03pm</code></strong> format</li>
 * </ol>
 * </p>
 * <p>
 * It also generates a JSON file, in the same format as the JSON input, which contains a suggested friends list
 * for each player.
 * </p>
 * <p>
 * <strong> This class should be invokable as an executable jar that requires no classpath</strong>. 
 * In other words, It should be possible to run this program from a command line with the following
 * command:
 * </p>
 * <p>
 *  <code>Usage: java -jar TestApp-0.0.1-SNAPSHOT.jar {input directory} {winners output file path} {suggested friends output file path}</code>
 * </p>
 */
public class Main {

	public static Gson GSON = new GsonBuilder().setPrettyPrinting().create();

	/**
	 * <p>
	 * This program expects/requires the following command-line arguments:
	 * </p>
	 * <p>
	 * <ol>
	 * 	<li>The (fully-qualified) path to a directory of (JSON) player input files</li>
	 *  <li>The (fully-qualified) path to the (CSV) output file containing a summary of player data in the format described above</li>
	 *  <li>The (fully-qualified) path to the (JSON) output file containing suggested friends data in the format described above</li>
	 * </ol>
	 * </p>
	 */
	public static void main(String[] args) {
		if (args.length < 3) {
			System.out.println("Usage: java -jar TestApp-0.0.1-SNAPSHOT.jar {input directory} {winners output file path} {suggested friends output file path}");
			System.exit(1);
		}

		System.out.println("TestApp process started at " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));

		String inputDirectoryPath = args[0];
		String winnersFilePath = args[1];
		String suggestedFriendsFilePath = args[2];

		File inputDirectory = new File(inputDirectoryPath);
		File winnersFile = new File(winnersFilePath);
		File suggestedFriendsFile = new File(suggestedFriendsFilePath);

		if (!inputDirectory.isDirectory()) {
			System.out.println(inputDirectoryPath + " is not a directory, exiting.");
			System.exit(2);
		}

		//read files from input directory
		List<PlayerDTO> players = readInputFiles(inputDirectory);

		//Attempt to write winners file
		try {
			List<PlayerDTO> winners = getWinners(players);
			writeWinSummaryFile(winners, winnersFile);
			System.out.println("Win summary written to " + winnersFile.getAbsolutePath());
		} catch (IOException e) {
			System.out.println("Couldn't write output file to " + winnersFile.getAbsolutePath());
			e.printStackTrace();
			System.exit(3);
		}

		//generate suggested friends data
		setSuggestedFriends(players);

		//Attempt to write suggested friends list
		try {
			writeSuggestedFriendsFile(players, suggestedFriendsFile);
			System.out.println("Suggested friends written to " + suggestedFriendsFile.getAbsolutePath());
		} catch (IOException e) {
			System.out.println("Couldn't write output file to " + suggestedFriendsFile.getAbsolutePath());
			e.printStackTrace();
			System.exit(4);
		}

		System.exit(0);
	}

	/**
	 * Reads a directory of player files into a List
	 * @param inputDirectory File representing directory containing JSON player files
	 * @return list of players read from input files
	 */
	public static List<PlayerDTO> readInputFiles(File inputDirectory) {
		List<PlayerDTO> players = new LinkedList<PlayerDTO>();

		for (File inputFile : inputDirectory.listFiles()) {
			System.out.println("Reading input file " + inputFile.getAbsolutePath());
			try {
				String data = readFromFile(inputFile);
				players.addAll((List<PlayerDTO>) GSON.fromJson(data, new TypeToken<List<PlayerDTO>>() {
				}.getType()));
			} catch (IOException e) {
				System.out.println("Couldn't read data from " + inputFile.getAbsolutePath());
				e.printStackTrace();
			}
		}

		return players;
	}

	/**
	 * Reads a single file into a String
	 * @param file Single JSON file of player data
	 * @return String containing file contents
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public static String readFromFile(File file) throws IOException {
		StringBuilder builder = new StringBuilder();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String currentLine = reader.readLine();

		while (currentLine != null) {
			builder.append(currentLine);
			currentLine = reader.readLine();
		}

		return builder.toString();
	}

	/**
	 * Write a CSV file of players that have more than 5 wins
	 * @param players List of players to filter and write to summary file
	 * @param outputFile File to write win summary data
	 * @throws IOException when unable to write to specified output
	 */
	@SuppressWarnings("resource")
	public static void writeWinSummaryFile(List<PlayerDTO> players, File outputFile) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));

		for (PlayerDTO currentPlayer : players) {
			if (currentPlayer.getNumWins() > 5) {
				writer.write(String.format(
					"%1$s, %2$s, %3$s, %4$tm/%4$td/%4$tY %4$tI:%4$tM%4$tp",
					currentPlayer.getPlayerId(),
					currentPlayer.getAlias(),
					currentPlayer.getNumWins(),
					currentPlayer.getLastPlayedDate()
				));
				writer.newLine();
			}
		}

		writer.flush();
	}

	/**
	 * Return a filtered list of players with more than 5 wins
	 * @param players List of players to filter
	 * @return filtered list of players
	 */
	public static List<PlayerDTO> getWinners(List<PlayerDTO> players) {
		List<PlayerDTO> winners = new LinkedList<PlayerDTO>();
		for (PlayerDTO p : players) {
			if (p.getNumWins() > 5)	{
				winners.add(p);
			}
		}
		return winners;
	}

	/**
	 * Writes a JSON file with friend suggestions
	 * @param players List of players, with friend lists populated with suggested friends
	 * @param outputFile File to write friend suggestions data
	 * @throws IOException When unable to write to specified output
	 */
	@SuppressWarnings("resource")
	public static void writeSuggestedFriendsFile(List<PlayerDTO> players, File outputFile) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));

		GSON.toJson(players, new TypeToken<List<PlayerDTO>>(){}.getType(), writer);

		writer.newLine();
		writer.flush();
	}

	/**
	 * Modifies the friends list of each PlayerDTO to a list of suggested friends, based on friends of current friends
	 * @param players List of players to analyze
	 */
	public static void setSuggestedFriends(List<PlayerDTO> players) {
		//Player lookup
		Map<Long, PlayerDTO> playersById = new HashMap<Long, PlayerDTO>(players.size());

		//Suggested friends for each player - initially empty
		Map<PlayerDTO, Set<PlayerDTO>> suggestedFriends = new HashMap<PlayerDTO, Set<PlayerDTO>>(players.size());

		//initialize player lookup and suggested friends set for each player
		for (PlayerDTO player : players) {
			playersById.put(player.getPlayerId(), player);
			suggestedFriends.put(player, new HashSet<PlayerDTO>(players.size()));
		}

		//build suggested friends set for each player
		for (PlayerDTO currentPlayer : playersById.values()) {
			List<PlayerDTO> currentPlayerFriends = currentPlayer.getFriendList();
			Set<PlayerDTO> currentPlayerSuggestedFriends = suggestedFriends.get(currentPlayer);

			if (currentPlayerFriends != null) {

				//make sure the player's first-level friends are in the suggested list
				currentPlayerSuggestedFriends.addAll(currentPlayerFriends);

				for (PlayerDTO friendOfPlayer : currentPlayerFriends) {

					//get the friend's original PlayerDTO (not from the friends list) for their friend list
					PlayerDTO friend = playersById.get(friendOfPlayer.getPlayerId());
					if (friend != null) {
						if (friend.getFriendList() != null) {
							currentPlayerSuggestedFriends.addAll(friend.getFriendList());

							//Uncomment next two lines to log which players gain suggested friends
							//if (currentPlayerSuggestedFriends.size() != currentPlayer.getFriendList().size())
							//	System.out.println(currentPlayer);
						}
					} else { //the friend wasn't in the file (outside a friend list)
						System.out.println(
							"Warning: player with playerId " +
								friendOfPlayer.getPlayerId() +
								" was listed as a friend, but was not present in the original player listing."
						);
					}
				}
			}
		}

		//set each player's friend list (in the original collection) to the set of suggested friends
		for (PlayerDTO player : players) {
			player.setFriendList(new LinkedList<PlayerDTO>(suggestedFriends.get(player)));
		}
	}

}
