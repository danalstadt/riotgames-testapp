package com.riotgames.devops.testapp;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * <p>
 * A DTO that tracks some data about a player
 * </p>
 */
@AllArgsConstructor
@Data
public class PlayerDTO {
	
	private String name;	//player name
	private String alias;   //player alias
	private long playerId;  //UNIQUE player ID
	private long numWins;	//number of games won by the player
	private long numLosses; //number of games lost by the player
	private Date createdDate; //date the player's account was created
	private Date lastPlayedDate; //date the player last played a game
	private List<PlayerDTO> friendList; //friends of the player
}
