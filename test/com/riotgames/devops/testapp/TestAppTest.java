package com.riotgames.devops.testapp;

import org.junit.*;

import java.io.File;
import java.util.*;

public class TestAppTest {

	File inputDir;
	List<PlayerDTO> players;

	@Before
	public void setUp() throws Exception {
		inputDir = new File("TestInput/");
		players = Main.readInputFiles(inputDir);
	}

	@Test
	public void testInput() {
		Assert.assertEquals("Should be five players", 5, players.size());
		Assert.assertEquals("Player 2 should have two friends", 2, players.get(1).getFriendList().size());
	}

	@Test
	public void testWinners() throws Exception {
		List<PlayerDTO> winners = Main.getWinners(players);
		Assert.assertEquals("Should be two winners", 2, winners.size());
	}

	@Test
	public void testSuggestedFriends() {
		Main.setSuggestedFriends(players);
		Assert.assertEquals("Player 1 should have three suggested friends", 3, players.get(0).getFriendList().size());
		Assert.assertEquals("Player 4 should have no suggested friends", 0, players.get(3).getFriendList().size());
		Assert.assertEquals("There should be 5 players after processing", 5, players.size());
	}
}
